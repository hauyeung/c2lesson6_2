using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Looping
{
    public partial class Looping : Form
    {
        private Label sentenceLabel;
        private TextBox sentenceTextBox;
        private Button countButton;
        private Label letterCountLabel;
        private Label reversedLabel;
        private TextBox reversedTextBox;
        private CheckBox animateCheckBox;
        private CheckBox reversecheckBox;
        bool doAnimation = false;

        public Looping()
        {
            InitializeComponent();
            reversecheckBox.Hide();
        }
   
        private void animateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            // Determine if we are to animate, then force paint event to occur using Refresh.
            doAnimation = animateCheckBox.Checked;
            this.Refresh();

        }


        private void InitializeComponent()
        {
            this.sentenceLabel = new System.Windows.Forms.Label();
            this.sentenceTextBox = new System.Windows.Forms.TextBox();
            this.countButton = new System.Windows.Forms.Button();
            this.letterCountLabel = new System.Windows.Forms.Label();
            this.reversedLabel = new System.Windows.Forms.Label();
            this.reversedTextBox = new System.Windows.Forms.TextBox();
            this.animateCheckBox = new System.Windows.Forms.CheckBox();
            this.reversecheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // sentenceLabel
            // 
            this.sentenceLabel.AutoSize = true;
            this.sentenceLabel.Location = new System.Drawing.Point(25, 13);
            this.sentenceLabel.Name = "sentenceLabel";
            this.sentenceLabel.Size = new System.Drawing.Size(81, 13);
            this.sentenceLabel.TabIndex = 0;
            this.sentenceLabel.Text = "Enter Sentence";
            // 
            // sentenceTextBox
            // 
            this.sentenceTextBox.Location = new System.Drawing.Point(28, 29);
            this.sentenceTextBox.Name = "sentenceTextBox";
            this.sentenceTextBox.Size = new System.Drawing.Size(219, 20);
            this.sentenceTextBox.TabIndex = 1;
            this.sentenceTextBox.TextChanged += new System.EventHandler(this.sentenceTextBox_TextChanged);
            // 
            // countButton
            // 
            this.countButton.Location = new System.Drawing.Point(28, 55);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(75, 23);
            this.countButton.TabIndex = 2;
            this.countButton.Text = "Count";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // letterCountLabel
            // 
            this.letterCountLabel.AutoSize = true;
            this.letterCountLabel.Location = new System.Drawing.Point(28, 94);
            this.letterCountLabel.Name = "letterCountLabel";
            this.letterCountLabel.Size = new System.Drawing.Size(67, 13);
            this.letterCountLabel.TabIndex = 3;
            this.letterCountLabel.Text = "Letter count:";
            // 
            // reversedLabel
            // 
            this.reversedLabel.AutoSize = true;
            this.reversedLabel.Location = new System.Drawing.Point(28, 121);
            this.reversedLabel.Name = "reversedLabel";
            this.reversedLabel.Size = new System.Drawing.Size(56, 13);
            this.reversedLabel.TabIndex = 4;
            this.reversedLabel.Text = "Reversed:";
            // 
            // reversedTextBox
            // 
            this.reversedTextBox.Location = new System.Drawing.Point(31, 137);
            this.reversedTextBox.Name = "reversedTextBox";
            this.reversedTextBox.Size = new System.Drawing.Size(216, 20);
            this.reversedTextBox.TabIndex = 5;
            // 
            // animateCheckBox
            // 
            this.animateCheckBox.AutoSize = true;
            this.animateCheckBox.Location = new System.Drawing.Point(138, 167);
            this.animateCheckBox.Name = "animateCheckBox";
            this.animateCheckBox.Size = new System.Drawing.Size(64, 17);
            this.animateCheckBox.TabIndex = 6;
            this.animateCheckBox.Text = "Animate";
            this.animateCheckBox.UseVisualStyleBackColor = true;
            this.animateCheckBox.CheckedChanged += new System.EventHandler(this.animateCheckBox_CheckedChanged_1);
            // 
            // reversecheckBox
            // 
            this.reversecheckBox.AutoSize = true;
            this.reversecheckBox.Location = new System.Drawing.Point(44, 167);
            this.reversecheckBox.Name = "reversecheckBox";
            this.reversecheckBox.Size = new System.Drawing.Size(66, 17);
            this.reversecheckBox.TabIndex = 7;
            this.reversecheckBox.Text = "Reverse";
            this.reversecheckBox.UseVisualStyleBackColor = true;
            this.reversecheckBox.CheckedChanged += new System.EventHandler(this.reversecheckBox_CheckedChanged);
            // 
            // Looping
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.reversecheckBox);
            this.Controls.Add(this.animateCheckBox);
            this.Controls.Add(this.reversedTextBox);
            this.Controls.Add(this.reversedLabel);
            this.Controls.Add(this.letterCountLabel);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.sentenceTextBox);
            this.Controls.Add(this.sentenceLabel);
            this.Name = "Looping";
            this.Text = "Looping";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Looping_FormClosing_1);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Looping_Paint_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void sentenceTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void countButton_Click(object sender, EventArgs e)
        {
            int letterCount = 0;

            // Use for loop to find and count letters only.
            for (int i = 0; i < sentenceTextBox.Text.Length; i++)
            {
                // Determine if we have a character.
                if (char.IsLetter(sentenceTextBox.Text, i))
                    letterCount++;
            }

            letterCountLabel.Text = "Letter count: " + letterCount;

            // Use while loop to reverse. 
            StringBuilder reverseSB = new StringBuilder(sentenceTextBox.Text.Length);

            int letterPosition = sentenceTextBox.Text.Length;
            while (letterPosition > 0)
            {
                // Add character, working from end of string backwards.
                reverseSB.Append(sentenceTextBox.Text.Substring(--letterPosition, 1));
            }

            reversedTextBox.Text = reverseSB.ToString();

        }

        private void animateCheckBox_CheckedChanged_1(object sender, EventArgs e)
        {
            if (animateCheckBox.Checked)
            {
                reversecheckBox.Show();               
            }
            else
            {
                reversecheckBox.Hide();
            }
            // Determine if we are to animate, then force paint event to occur using Refresh.
            doAnimation = animateCheckBox.Checked;
            this.Refresh();                        
        }

        private void Looping_Paint_1(object sender, PaintEventArgs e)
        {
            if (reversecheckBox.Checked)
            {
                string drawText = sentenceTextBox.Text.Length > 0 ? sentenceTextBox.Text : "Some text";
                // Determine our approximate stopping point.
                float maxWidth = e.Graphics.VisibleClipBounds.Width;
                // Set up starting x and y locations, noting that only the x is going to change.
                float x = animateCheckBox.Right;
                float y = animateCheckBox.Top + (animateCheckBox.Height * 2);
                // We'll need a previous x location so we can 'erase' the previous drawing.
                float previousX = x;

                // Loop until either we've reached our stopping point, or doAnimation is false.
                do
                {
                    // Draw text at old location.
                    e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(this.BackColor), previousX, y);
                    // Make the system sleep for a short time so we can see the animation.
                    System.Threading.Thread.Sleep(20);
                    // Allow the program to process events so we can allow the user interface to be responsive after sleeping.
                    Application.DoEvents();
                    // Draw text at new location.
                    e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(Color.Red), x, y);
                    // Save the current location.
                    previousX = x;
                    // Advance to next location.
                    x--;
                } while (x >= maxWidth && doAnimation);

                // Do some cleanup.
                e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(this.BackColor), previousX, y);
                //animateCheckBox.Checked = false;
                reversecheckBox.Show();
                doAnimation = false;
                
            }
            else
            {
                string drawText = sentenceTextBox.Text.Length > 0 ? sentenceTextBox.Text : "Some text";
                // Determine our approximate stopping point.
                float maxWidth = e.Graphics.VisibleClipBounds.Width;
                // Set up starting x and y locations, noting that only the x is going to change.
                float x = animateCheckBox.Left;
                float y = animateCheckBox.Top + (animateCheckBox.Height * 2);
                // We'll need a previous x location so we can 'erase' the previous drawing.
                float previousX = x;

                // Loop until either we've reached our stopping point, or doAnimation is false.
                do
                {
                    // Draw text at old location.
                    e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(this.BackColor), previousX, y);
                    // Make the system sleep for a short time so we can see the animation.
                    System.Threading.Thread.Sleep(20);
                    // Allow the program to process events so we can allow the user interface to be responsive after sleeping.
                    Application.DoEvents();
                    // Draw text at new location.
                    e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(Color.Red), x, y);
                    // Save the current location.
                    previousX = x;
                    // Advance to next location.
                    x++;
                } while (x < maxWidth && doAnimation);

                // Do some cleanup.
                e.Graphics.DrawString(drawText, new Font("Arial", 8), new SolidBrush(this.BackColor), previousX, y);
                //animateCheckBox.Checked = false;
                doAnimation = false;
            }

        }

        private void Looping_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            // We need to prevent, or cancel, closing, as code may still be in animation do loop.
            if (doAnimation)
            {
                MessageBox.Show("You must stop any animation before trying to close the program", this.Text, MessageBoxButtons.OK);
                e.Cancel = true;
            }

        }

        private void reversecheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (reversecheckBox.Checked)
            {
                this.Refresh();
            }
        }
        }
    }

      